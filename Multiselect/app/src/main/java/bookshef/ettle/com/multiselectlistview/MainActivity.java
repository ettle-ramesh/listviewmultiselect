package bookshef.ettle.com.multiselectlistview;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import bookshef.ettle.com.multiselectlist.R;

public class MainActivity extends AppCompatActivity {
    private ListView mListView;
    ArrayList<String> mItems = new ArrayList<String>();
    private int count;
    private boolean[] mArraySelection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadArray();
        count = mItems.size();
        mArraySelection = new boolean[count];
        mListView = (ListView) findViewById(R.id.listView1);
        mListView.setAdapter(new ListAdapter(MainActivity.this));
    }

    private void loadArray() {
        // TODO Auto-generated method stub
        mItems.clear();
        mItems.add("Android alpha");
        mItems.add("Android beta");
        mItems.add("1.5 Cupcake (API level 3)");
        mItems.add("1.6 Donut (API level 4)");
        mItems.add("2.0 Eclair (API level 5)");
        mItems.add("2.0.1 Eclair (API level 6)");
        mItems.add("2.1 Eclair (API level 7)");
        mItems.add("2.2–2.2.3 Froyo (API level 8)");
        mItems.add("2.3–2.3.2 Gingerbread (API level 9)");
        mItems.add("2.3.3–2.3.7 Gingerbread (API level 10)");
        mItems.add("3.0 Honeycomb (API level 11)");
        mItems.add("3.1 Honeycomb (API level 12)");
        mItems.add("3.2 Honeycomb (API level 13)");
        mItems.add("4.0–4.0.2 Ice Cream Sandwich (API level 14)");
        mItems.add("4.0.3–4.0.4 Ice Cream Sandwich (API level 15)");
        mItems.add("4.1 Jelly Bean (API level 16)");
        mItems.add("4.2 Jelly Bean (API level 17)");
        mItems.add("5.0 Key Lime Pie (API level 18)");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds mItems to the action bar if it is present.
        return true;
    }

    public class ListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private Context mContext;

        public ListAdapter(Context context) {
            mContext = context;
        }

        public int getCount() {
            return count;
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(mContext).inflate(
                        R.layout.row_photo, null);
                holder.textview = (TextView) convertView
                        .findViewById(R.id.thumbImage);
                holder.checkbox = (CheckBox) convertView
                        .findViewById(R.id.itemCheckBox);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.checkbox.setId(position);
            holder.textview.setId(position);
            holder.checkbox.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    CheckBox cb = (CheckBox) v;
                    int id = cb.getId();
                    if (mArraySelection[id]) {
                        cb.setChecked(false);
                        mArraySelection[id] = false;
                    } else {
                        cb.setChecked(true);
                        mArraySelection[id] = true;
                    }
                }
            });
            holder.textview.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    int id = v.getId();
                }
            });
            holder.textview.setText(mItems.get(position));
            holder.checkbox.setChecked(mArraySelection[position]);
            holder.id = position;
            return convertView;
        }
    }

    class ViewHolder {
        TextView textview;
        CheckBox checkbox;
        int id;
    }

    public void click(View v) {
        if (v.getId() == R.id.button1) {
            final ArrayList<Integer> posSel = new ArrayList<Integer>();
            posSel.clear();
            boolean noSelect = false;
            for (int i = 0; i < mArraySelection.length; i++) {
                if (mArraySelection[i] == true) {
                    noSelect = true;
                    Log.e("sel pos thu-->", "" + i);
                    posSel.add(i);
                    // break;
                }
            }
            if (!noSelect) {
                Toast.makeText(MainActivity.this, "Please Select Item!",
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this,
                        "Selected Items:" + posSel.toString(),
                        Toast.LENGTH_LONG).show();
            }
        }
    }
}
